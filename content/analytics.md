---
title: Analytics 📈
description: We publish open analytics as part of our commitment to transparency!
---

#### **_Provided by_**

![Plausible Analytics](/tech-stack/plausible-logo.png)

## Landing Website

<iframe plausible-embed src="https://plausible.io/share/gsoftware.org?auth=k8ybZUWqj4n7wvl26NhNI&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/gsoftware.org)**

## Org Website

<iframe plausible-embed src="https://plausible.io/share/org.gsoftware.org?auth=cQh8gVI_y8KOiczjrwmfw&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/org.gsoftware.org)**

## Learn

<iframe plausible-embed src="https://plausible.io/share/learn.gsoftware.org?auth=zgkaBtGCGYoRfQ_j_ZyRR&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

**[Full Analytics](https://plausible.io/learn.gsoftware.org)**

## Resources

<iframe plausible-embed src="https://plausible.io/share/resources.gsoftware.org?auth=Z8FYqPyCvyAbimDtTQN8K&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/resources.gsoftware.org)

## Onboarding

<iframe plausible-embed src="https://plausible.io/share/onboarding.gsoftware.org?auth=PPr6299z_kbD_AweU0f88&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/onboarding.gsoftware.org)

## Glossary

<iframe plausible-embed src="https://plausible.io/share/glossary.gsoftware.org?auth=6TBqBDbrKAcJe7xqwiI0Y&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/glossary.gsoftware.org)

## Material Math

<iframe plausible-embed src="https://plausible.io/share/material-math.gsoftware.org?auth=R4BQcZ1FJXAQmpkuSobSX&embed=true&theme=light" scrolling="yes" frameborder="0" loading="lazy" style="width: 1px; min-width: 100%; height: 550px;"></iframe>

[Full Analytics](https://plausible.io/material-math.gsoftware.org)
