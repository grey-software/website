---
title: Pitch
description: Check out Grey Software's startup pitch!
---

> **Goal**: Raise 50k+ non-equity dilutive capital
>
> **Why**: To catalyze our path to sustainability
>
> **How**: Offer lifetime recognition to Angel Investors, VC companies, and other organizations who would like to
> feature their brand on our websites and repositories as Founding Partners.

<details>
    <summary>Notes</summary>

### Detail your value proposition

> **For** (target customers)  
> **Who are dissatisfied with** (the current alternative)  
> **Our product is a** (new product)  
> **That provides** (essential problem-solving capability)  
> **Unlike** (the product alternative)

**For** Angel investors and VC firms

**Who are dissatisfied** with how the status quo is not optimally preparing the next generation to create or scale
software products

**Grey Software** is making open software products and publishing what we learn along the way in an organized education
ecosystem

**So that** young, aspiring software entrepreneurs around the world can have access to authentic, up-to-date, and
practical software education

**Unlike** the confusing web of outtdated university curriculums, selective internships, expensive bootcamps or online
courses, and free but unorganized resources

### Impact & Traction

#### Malpani Ventures Grant

- Dr. Malpani from Malpani ventures authorized two grants totalling 130,000 INR after he announced he was funding
  open-source edtech ventures in India on Twitter!

#### Open Core Summit Grant

- Joseph Jacks from the Open Core Summit authorized a $1000 CAD grant after I made my case on Twitter and LinkedIn!

### Risk

#### What are the key risks that would be involved in making this investment?

- After using all the money from its founding investments, the **organization is still unable to sustain itself**
  through its revenue channels.

#### What plans are in place to mitigate thetse risks in the next period?

- [Public weekly updates](https://org.gsoftware.org/this-week/10-04-2021/)

</details>

## Students Are Our Future, But They're Falling Behind

![Intro Slide](/pitch/1.png)

## Tech Talent Projections Are Concerning

![Concerning Future Projections Slide](/pitch/2.png)

<details>
    <summary>Notes & Sources</summary>

### KornFerry Report on the Talent Crunch in the Future of Work

KornFerry's report analyzes 20 of the world's largest economies and paints a concerning picture.

By 2030, talent shortages slow the digital revolution due to a potential labor skills shortage of 4.3 million workers
and unrealized output of $449.70 billion.

<cta-button text="Full Report" link="https://www.kornferry.com/content/dam/kornferry/docs/pdfs/KF-Future-of-Work-Talent-Crunch-Report.pdf"></cta-button>

### Skill shortages are preventing organisations from keeping up with the pace of change

This data comes from The Harvey Nash / KPMG CIO Survey, one of the world’s largest studies of IT leadership. We refer to
the freely available 2019 survey, and **we projected that the percentage of organizations struggling to keep up with the
pace of change due to skill shortages stayed above 60% throughout 2020 and 2021.**

![](https://i.imgur.com/huc4qKd.png)

<cta-button text="Source" link="https://www.hnkpmgciosurvey.com/charts/home"></cta-button>

</details>

## The Status Quo is Shaky

![Status Quo Slide](/pitch/3.png)

## Introducing Grey Software

![Introducing Grey Software Slide](/preview.png)

## The Competition

![Slide 18](/pitch/18.png)

<details>
    <summary>Expand To Learn More About Our Competition</summary>

- Mozilla creates high quality open source products to promote internet freedom, and manages the Mozilla Developer
  Network which is one of the world’s best sources for web development education.

- FreeCodeCamp is a not-for-profit company that creates videos, articles, and interactive coding lessons - all freely
  available to the public.
- Khan Academy is a nonprofit with the mission of providing a free, world-class education for anyone, anywhere. They
  have Courses about computer science but don’t delve as deep as Free Code Camp with respect to industry software
  engineer and data science.
- MLH has an excellent partnership network with universities and tech companies around the world. With this, they run
  events, competitions, and open source apprenticeship programs for students get practical software development
  experience.

</details>

## Testimonials

![Testimonials Slide](/pitch/4.png)

## The Key Insight

![Key Insight Slide](/pitch/5.png)

## Our Strategy

![Strategy Slide](/pitch/6.png)

## Strategy: Open Software

![Slide 25](/pitch/25.png)

<details>
    <summary>Expand To Learn More About Our Open Software Strategy</summary>

### Personas

#### Yasmine The University Student

Yasmine is a university computer science student who is actively engaged in academic and creative work online.

She uses independently developed apps and utilities like News Feed Eradicator, Tab Brightness Controller, and others.

Yasmine is comfortable with paying for software if it benefits her life, and she is likely to support a trusted
organization building a suite of useful apps and utilities for people like her.

#### Jerry The Enthusiast

Jerry has been working with computers for most of his life, and he has crafted a good living situation for himself from
his time in the technology industry.

Jerry has seeing the evolution of computer and software technology. That is why he respects and sometimes financially
supports institutions like the Apache Software Foundation, the Free Software Foundation, Mozilla, and other impactful
tech organizations who have benefited society.

### Hypothesis

People who do creative, academic, or professional work on the web want to use apps and utilities that are actively
supported by a trusted brand rather than independently developed apps and utilities with no guarantee of bug fixes and
new features.

If we create a collection software apps and utilities with a generous amount of free features and offer premium
functionality for a monthly subscription, we can generate sustainable revenue for Grey Software.

### Value proposition

If you do creative, academic, or professional work on the web, you probably use some independently developed apps or
utilities which don't come from a trusted brand that offers support or actively develops feature requests.

Instead, Grey Software offers a growing collection of freemium software apps and utilities that are actively supported
by a trusted brand.

### Detailed Strategy

- We build useful open source products for the public
- We generate revenue by offering premium features on our products for sponsors.
- We aim to update our catalog of apps through signing existing open source apps and offering students the chance to
  incubate a product idea with us and publish it on our collection
- If we offer a valuable collection of products for a monthly subscription, we can achieve economies of scale like
  Google GSuite, Microsoft Office365, or Adobe Creative Cloud, but for general public consumer software.

</details>

## Strategy: Open Education

![Slide 26](/pitch/26.png)

<details>
    <summary>Expand To Learn More About Our Open Education Strategy</summary>

### Detailed value proposition

> **For** (target customers)  
> **Who are dissatisfied with** (the current alternative)  
> **Our product is a** (new product)  
> **That provides** (essential problem-solving capability)  
> **Unlike** (the product alternative)

**For** university students

**Who are dissatisfied** with how confusing it is to get trustworthy and up-to-date software guidance

Grey Software is an organization of **passionate software creators and educators**

Who **build open products** and **share educational content** to help **students worldwide learn with us**

**Unlike outdated** university curriculums **and scattered** online software guidance

</details>

## Strategy: Open Collaboration

![Slide 27](/pitch/27.png)

<details>
    <summary>Expand To Learn More About Our Open Collaboration Strategy</summary>

### What is your value proposition?

> **For** (target customers)  
> **Who are dissatisfied with** (the current alternative)  
> **Our product is a** (new product)  
> **That provides** (essential problem-solving capability)  
> **Unlike** (the product alternative)

**For** university students

**Who are dissatisfied** with how their outdated curriculums are not effectively preparing them to create or scale
software products

**Grey Software is** an organization of passionate software creators and educators

Who build open products and publish educational content to **help students worldwide learn by example**

Unlike universities that **can't teach students** how to build up-to-date software products **because they aren't in the
business of creating products**

### What highlights from customer feedback validate the effectiveness of your go-to-market approach and business model?

I have been programming on the side for the past year now, mainly using online resources to pair program... yet one
session with my mentor was more enriching than hours spent on these side tutorials. - **Osama, Grey Software Explorer**

I got to experience what it was like being onboarded into a codebase and collaborating with a skilled developer and
designer. I got the software development education I was looking for." - **Milind, Grey Software Apprentice**

This course simulated what it will be like to work in the industry by encouraging the students to work asynchronously
and proactively rather than following the instructions of an assignment. - **Shawn, Student at UTM**

I had to get used to a different mindset; working asynchronously and making progress daily instead of cramming before a
deadline. - **Lee, Student at UTM**

The most useful was the overall workflow because it can be applied to any future project. All projects need proper
documentation and remote repo, and group members need to work asynchronously by managing branches, pull requests, issues
and milestones. **- Baichen, Student at UTM**

</details>

## Our Impact

![Impact Slide](/pitch/7.png)

## Impact: Open Software

![Slide 30](/pitch/30.png)

## Impact: Open Education

![Slide 31](/pitch/31.png)

## Impact: Open Collaboration

![Slide 32](/pitch/32.png)

## Our Revenue Roadmap

![Revenue Roadmap Slide](/pitch/8.png)

## Become a Founding Partner

![Become a Founding Partner Slide](/pitch/10.png)

## Who You're Backing

![Who You're Backing Slide](/pitch/9.png)

### Thank You

We appreciate you taking the time to go through our pitch 😃

If you would like to become a founding partner, please reach out to us at our org@ email!

## Thank You

We appreciate you taking the time to go through our pitch 😃

If you would like to proceed, please reach out to us at our org@ email!
