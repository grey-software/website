---
title: Pitch
description: Check out Grey Software's startup pitch!
---

> **Goal**: Raise 50k+ non-equity dilutive capital
>
> **Why**: To catalyze our path to sustainability
>
> **How**: Offer lifetime recognition to Angel Investors, VC companies, and other organizations who would like to
> feature their brand on our websites and repositories as Founding Partners.

## Students Are Our Future, But They're Falling Behind

![Intro Slide](/pitch/1.png)

## Tech Talent Projections Are Concerning

![Concerning Future Projections Slide](/pitch/2.png)

<details>
    <summary>Notes & Sources</summary>

### KornFerry Report on the Talent Crunch in the Future of Work

KornFerry's report analyzes 20 of the world's largest economies and paints a concerning picture.

By 2030, talent shortages slow the digital revolution due to a potential labor skills shortage of 4.3 million workers
and unrealized output of $449.70 billion.

<cta-button text="Full Report" link="https://www.kornferry.com/content/dam/kornferry/docs/pdfs/KF-Future-of-Work-Talent-Crunch-Report.pdf"></cta-button>

### Skill shortages are preventing organisations from keeping up with the pace of change

This data comes from The Harvey Nash / KPMG CIO Survey, one of the world’s largest studies of IT leadership. We refer to
the freely available 2019 survey, and **we projected that the percentage of organizations struggling to keep up with the
pace of change due to skill shortages stayed above 60% throughout 2020 and 2021.**

![](https://i.imgur.com/huc4qKd.png)

<cta-button text="Source" link="https://www.hnkpmgciosurvey.com/charts/home"></cta-button>

</details>

## The Status Quo is Shaky

![Status Quo Slide](/pitch/3.png)

## The Competitive Landscape

![Status Quo Slide](/pitch/18.png)

## Introducing Grey Software

![Introducing Grey Software Slide](/preview.png)

## Our Strategy

![Strategy Slide](/pitch/6.png)

## Open Software Strategy

<cta-button link="https://org.gsoftware.org/revenue/app-collection" text="Details"></cta-button><cta-button link="https://gitlab.com/groups/grey-software/-/epics/3" text="Gitlab Epic"></cta-button>

![Strategy Slide](/pitch/6a.png)

## Open Education Strategy

<cta-button link="https://org.gsoftware.org/revenue/ecosystem-partners" text="Details"></cta-button><cta-button link="https://gitlab.com/groups/grey-software/-/epics/17" text="Gitlab Epic"></cta-button>

![Strategy Slide](/pitch/6b.png)

## Open Collaboration Strategy

<cta-button link="https://org.gsoftware.org/revenue/edu-programs" text="Details"></cta-button><cta-button link="https://gitlab.com/groups/grey-software/-/epics/25" text="Gitlab Epic"></cta-button>

![Strategy Slide](/pitch/6c.png)

## The Key Insight

![Key Insight Slide](/pitch/5.png)

## Our Impact

<cta-button link="/analytics" text="Analytics"></cta-button>

![Impact Slide](/pitch/7.png)

## Testimonials

<cta-button link="/feedback" text="Feedback"></cta-button>

![Testimonials Slide](/pitch/4.png)

## Our Revenue Roadmap

![Revenue Roadmap Slide](/pitch/8.png)

## Our Team

<cta-button link="/team" text="Our Team"></cta-button>

![Who You're Backing Slide](/pitch/9.png)

## Become a Founding Partner

![Become a Founding Partner Slide](/pitch/10.png)

### Thank You

We appreciate you taking the time to go through our pitch 😃

If you would like to become a founding partner, please reach out to us at our org@ email!
