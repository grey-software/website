---
title: Hacktoberfest 2021
description: Grey software is participating in  Hacktoberfest 2021! Learn more about how you can contribute!
---

![Preview](https://hacktoberfest.digitalocean.com/_nuxt/img/logo-hacktoberfest-full.f42e3b1.svg)

<cta-button text="Sign Up" link="https://hacktoberfest.digitalocean.com/register"></cta-button>
<cta-button text="Open Issues" link="https://gitlab.com/groups/grey-software/-/issues?scope=all&state=opened&label_name[]=Hacktoberfest"></cta-button>

Hacktoberfest is a month-long global celebration of open source software run by DigitalOcean in partnership with Intel,
AppWrite, and DeepSource, with a strong focus on encouraging contributions to open source projects.

- Hacktoberfest is open to everyone.

- Four quality pull requests must be submitted to public GitHub and/or GitLab repositories.

- You can sign up anytime between October 1 and October 31.

<cta-button text="Learn More" link="https://hacktoberfest.digitalocean.com/faq"></cta-button>

## Prizes

You can earn a limited-edition Hacktoberfest t-shirt or
[have a tree planted in your name by Tree-Nation](https://tree-nation.com/profile/digitalocean).

### Contributor Criteria

If you are a contributor, you earn a prize for submitting 4 valid pull requests.

### Maintainer Criteria

If you are a maintainer, you earn a prize for showing that you completed 4 or more actions on unique pull requests.

## Get Started

We recommend that you get started with completing our onboarding exercise and signing yourself up as a Grey Software
explorer.

You **will get hacktoberfest credit** for onboarding successfully!

<cta-button text="Start Onboarding" link="https://onboarding.gsoftware.org"></cta-button>

## Projects

## Grey Software Resources

![Grey Software Resources Preview](https://i.imgur.com/k5TnuEL.png)

<cta-button text="Open Issues" link="https://gitlab.com/grey-software/resources/-/issues?scope=all&state=opened&label_name[]=Hacktoberfest"></cta-button>

## Focused Browsing

![Focused Browsing Preview](https://gsoftware.org/focused-browsing/preview.png)

<cta-button text="Open Issues" link="https://gitlab.com/grey-software/focused-browsing/-/issues?scope=all&state=opened&label_name[]=Hacktoberfest"></cta-button>

## Grey Software Landing

![Grey Software Landing Preview](https://gsoftware.org/preview.png)

<cta-button text="Open Issues" link="https://gitlab.com/grey-software/website/-/issues?scope=all&state=opened&label_name[]=Hacktoberfest"></cta-button>
